export const getContacts = () => {
  const contactApi = "https://tecnical-test-3dves-csxp4jntj.now.sh/contacts";
  const contacts = fetch(contactApi).then(response =>
    response.json().then(data => data)
  );
  return contacts;
};

export const createContacts = async body => {
  const contactApi =
    "https://tecnical-test-3dves-csxp4jntj.now.sh/contacts/create";
  const click = await fetch(contactApi, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
  }).then(response => response.json().then(data => data.info));

  return click;
};

export const updateContact = async body => {
  const contactApi =
    "https://tecnical-test-3dves-csxp4jntj.now.sh/contacts/update";
  const click = await fetch(contactApi, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
  }).then(response => response.json().then(data => data.info));
  return click;
};

export const deleteContact = async id => {
  const contactApi =
    "https://tecnical-test-3dves-csxp4jntj.now.sh/contacts/delete";
  const click = await fetch(contactApi, {
    method: "DELETE",
    mode: "cors",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ id })
  }).then(response => response.json().then(data => data.info));

  return click;
};

export const getSingleContact = async id => {
  const contactApi = `https://tecnical-test-3dves-csxp4jntj.now.sh/contacts/update/${id}`;
  const contact = await fetch(contactApi).then(response =>
    response.json().then(data => data)
  );
  return contact;
};
