import List from '../components/pages/List/List'
import Update from '../components/pages/Update/Update'
import Create from '../components/pages/Create/Create'

export const routes = [
  {
    exact: true,
    path: '/',
    component: List,
  },
  {
    exact: true,
    path: '/create',
    component: Create,
  },
  {
    exact: true,
    path: '/update/:id',
    component: Update,
  },
]

export default routes