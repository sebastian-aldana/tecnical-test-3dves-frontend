import React from 'react'
import { Switch, Route, } from 'react-router-dom'
import { routes } from './routes-list'


const App = () => {
  const renderRoutes = (route) => (
    <Route
      key={route.path}
      path={route.path}
      exact={route.exact}
      component={route.component}
    />
  )
  return (
    <Switch>
      {routes.map(renderRoutes)}
    </Switch>
  )
}

export default App