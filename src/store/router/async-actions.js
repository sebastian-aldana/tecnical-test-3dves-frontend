import { push } from "connected-react-router";
import { cleanContactToEdit } from "../../store/contacts/actions";
import { getSingleContactAsync } from "../contacts/asyn-actions";

export const pushUpdate = id => {
  return async dispatch => {
    try {
      await dispatch(getSingleContactAsync(id));
      dispatch(push(`/update/${id}`));
    } catch (error) {
      console.log(error);
    }
  };
};

export const pushCreate = () => {
  return async dispatch => {
    try {
      await dispatch(cleanContactToEdit());
      dispatch(push("/create"));
    } catch (error) {
      console.log(error);
    }
  };
};

export const pushList = () => {
  return async dispatch => {
    try {
      await dispatch(push("/"));
    } catch (error) {
      console.log(error);
    }
  };
};
