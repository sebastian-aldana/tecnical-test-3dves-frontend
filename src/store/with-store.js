import React from "react";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import mock from "./mock-store";

function withStore(component) {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);

  const store = mockStore({ ...mock });

  return <Provider store={store}>{component}</Provider>;
}

export default withStore;
