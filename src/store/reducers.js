import { combineReducers } from "redux";
import { contactsReducer } from "./contacts";
import { connectRouter } from "connected-react-router";

export const AppReducers = history =>
  combineReducers({
    contacts: contactsReducer,
    router: connectRouter(history)
  });
