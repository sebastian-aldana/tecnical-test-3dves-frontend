export function getInitial(state) {
  return state.contacts;
}

export function getContacts(state) {
  return getInitial(state).contacts;
}

export function getUpdatedContact(state) {
  return getInitial(state).updateContact;
}
