export function contactsReducer(
  state = {
    contacts: [],
    updateContact: []
  },
  action
) {
  switch (action.type) {
    case "@@CONTACTS/GET":
      return {
        ...state,
        contacts: action.payload
      };
    case "@@CONTACT/SET_UPDATE_CONTACT":
      return {
        ...state,
        updateContact: action.payload
      };
    case "@@CONTACT/SET_CLEAN_UPDATE_CONTACT":
      return {
        ...state,
        updateContact: []
      };

    default:
      return state;
  }
}
