export const getContactsAction = contacts => {
  return dispatch => {
    dispatch({
      type: "@@CONTACTS/GET",
      payload: contacts
    });
  };
};

export const setContactToEdit = contact => {
  return dispatch => {
    dispatch({
      type: "@@CONTACT/SET_UPDATE_CONTACT",
      payload: contact
    });
  };
};

export const cleanContactToEdit = () => {
  return dispatch => {
    dispatch({
      type: "@@CONTACT/SET_CLEAN_UPDATE_CONTACT"
    });
  };
};
