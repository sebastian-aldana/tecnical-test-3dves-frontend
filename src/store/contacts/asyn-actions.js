import {
  getContacts,
  createContacts,
  updateContact,
  deleteContact,
  getSingleContact
} from "../../services/contacts";
import {
  getContactsAction,
  setContactToEdit,
  cleanContactToEdit
} from "./actions";

export const getContactsAsync = () => {
  return async dispatch => {
    try {
      const response = await getContacts();
      dispatch(getContactsAction(response));
    } catch (error) {
      console.log(error);
    }
  };
};

export const createContactAsync = body => {
  return async dispatch => {
    try {
      const response = await createContacts(body);
    } catch (error) {
      console.log(error);
    }
  };
};

export const updateContactAsync = body => {
  return async dispatch => {
    try {
      const response = await updateContact(body);
      dispatch(cleanContactToEdit());
    } catch (error) {
      console.log(error);
    }
  };
};

export const deleteContactAsync = id => {
  return async dispatch => {
    try {
      const response = await deleteContact(id);
    } catch (error) {
      console.log(error);
    }
  };
};

export const getSingleContactAsync = id => {
  return async dispatch => {
    try {
      const response = await getSingleContact(id);
      dispatch(setContactToEdit(response));
    } catch (error) {
      console.log(error);
    }
  };
};
