import { createStore, applyMiddleware, compose } from "redux";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import { AppReducers } from "./reducers";

export const history = createBrowserHistory();

const persisConfig = {
  key: "test",
  storage
};

const persistedReducer = persistReducer(persisConfig, AppReducers(history));

export const store = createStore(
  persistedReducer,
  compose(applyMiddleware(thunk, routerMiddleware(history)))
);

export const persistor = persistStore(store);
