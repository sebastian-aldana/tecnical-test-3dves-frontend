import React from 'react';
import { useDispatch } from 'react-redux'
import { pushCreate } from '../../../store/router/async-actions'
import { Button } from 'semantic-ui-react'

const CreateButtonPush = () => {
  const dispatch = useDispatch()
  const pushCreateAction = () => {
    dispatch(pushCreate())
  }
  return (
    <Button color="green" onClick={pushCreateAction} >Crear</Button>
  )
}

export default CreateButtonPush