import React from "react";
import { render } from "enzyme";
import CreateComponet from "./create-button";
import withStore from "../../../store/with-store";

describe("create button component", () => {
  test("should render component", done => {
    const wrapper = render(withStore(<CreateComponet />));
    expect(wrapper).toMatchSnapshot();
    done();
  });
});
