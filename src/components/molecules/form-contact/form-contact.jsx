import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { createContactAsync, updateContactAsync } from '../../../store/contacts/asyn-actions'
import { useSelector } from 'react-redux';
import { pushList } from '../../../store/router/async-actions'
import { getUpdatedContact, getContacts } from '../../../store/contacts/selectors'
import { Button, Checkbox, Form } from 'semantic-ui-react'


const FormContact = (props) => {
  const dispatch = useDispatch()
  const contactSelected = useSelector(getUpdatedContact)
  const contacts = useSelector(getContacts)
  const [body, setBody] = useState({
    id: contactSelected ? contactSelected.id : contacts.length,
    name: contactSelected ? contactSelected.name : '',
    lastName: contactSelected ? contactSelected.lastName : '',
    phone: contactSelected ? contactSelected.phone : ''
  })
  const diccionario = {
    "/create": createContactAsync(body),
    "/update/:id": updateContactAsync(body)
  }
  const click = async () => {
    await dispatch(diccionario[props.match.path])
    dispatch(pushList())
  }
  const changeBody = (e) => {
    const { name, value } = e.target
    setBody({ ...body, [name]: value })
  }
  return (
    <>
      <Form>
        <Form.Field width='6'>
          <label htmlFor="">Nombre</label>
          <input name="name" onChange={changeBody} type="text" value={body.name} />
        </Form.Field >
        <Form.Field width='6' >
          <label htmlFor="">Apellido</label>
          <input name="lastName" onChange={changeBody} type="text" value={body.lastName} />
        </Form.Field>
        <Form.Field width='6' >
          <label htmlFor="">telefono</label>
          <input name="phone" onChange={changeBody} type="number" value={body.phone} />
        </Form.Field>
        <Button color="green" onClick={click} >Enviar</Button>
      </Form>
    </>
  )
}

export default withRouter(FormContact)