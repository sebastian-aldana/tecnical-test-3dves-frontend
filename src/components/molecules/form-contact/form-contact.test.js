import React from "react";
import { render } from "enzyme";
import FormContact from "./form-contact";
import withStore from "../../../store/with-store";
import { withRouter } from "react-router-dom";

describe("form contact component", () => {
  test("should render component ", done => {
    const wrapper = render(withStore(withRouter(<FormContact />)));
    expect(wrapper).toMatchSnapshot();
    done();
  });
});
