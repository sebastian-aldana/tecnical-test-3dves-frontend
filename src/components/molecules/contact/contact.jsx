import React from 'react';
import { useDispatch } from 'react-redux'
import { deleteContactAsync } from '../../../store/contacts/asyn-actions'
import { getContactsAsync } from '../../../store/contacts/asyn-actions'
import { pushUpdate } from '../../../store/router/async-actions'
import { Button, Card } from 'semantic-ui-react'

const Contact = ({ contact }) => {
  const dispatch = useDispatch()
  const deleteContact = async (e) => {
    const { id } = e.target
    await dispatch(deleteContactAsync(id))
    dispatch(getContactsAsync())
  }
  const updatePush = (e) => {
    dispatch(pushUpdate(e.target.id))
  }
  return (
    <Card>
      <Card.Content>
        <Card.Header>{`Nombre: ${contact.name}`}</Card.Header>
        <Card.Meta> {`Apellido: ${contact.lastName}`}</Card.Meta>
        <Card.Meta> {`Telefono: ${contact.phone}`}</Card.Meta>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green' id={contact.id} onClick={updatePush} >
            Editar
          </Button>
          <Button basic color='red' id={contact.id} onClick={deleteContact}>
            Eliminar
          </Button>
        </div>
      </Card.Content>
    </Card>
  )
}

export default Contact