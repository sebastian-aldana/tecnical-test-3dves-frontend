import React from "react";
import { shallow } from "enzyme";
import List from "./List";
import withStore from "../../../store/with-store";

describe("form contact component", () => {
  test("should render component ", done => {
    const wrapper = shallow(withStore(<List />));
    expect(wrapper).toMatchSnapshot();
    done();
  });
});
