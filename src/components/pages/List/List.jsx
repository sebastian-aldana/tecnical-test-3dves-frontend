import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { getContacts } from '../../../store/contacts/selectors'
import { useDispatch } from 'react-redux'
import { getContactsAsync } from '../../../store/contacts/asyn-actions'
import Contact from '../../molecules/contact/contact'
import CreateButtonPush from '../../atoms/create-button/create-button'
import { Card, Header } from 'semantic-ui-react'

const list = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getContactsAsync())
  }, [])
  const contacts = useSelector(getContacts)
  const pushCreate = () => {
    dispatch(pushCreate())
  }
  contacts.length !== 0 ? (
    <h1>No hay contactos</h1>
  ) : ''
  return (
    <>
      <Header as="h2">Bienvenido a la guia de contactos</Header>
      <Header as="h3">contactos</Header>
      {contacts.length === 0 ? (
        <Header as="h4">No hay contactos</Header>
      ) : (
          <Card.Group>
            {contacts.map(contact => (
              <Contact key={contact.id} contact={contact} />
            ))}
          </Card.Group>
        )}
      <CreateButtonPush />
    </>

  )
}


export default list