import React from "react";
import { shallow } from "enzyme";
import Create from "./Create";
import withStore from "../../../store/with-store";

describe("form contact component", () => {
  test("should render component ", done => {
    const wrapper = shallow(withStore(<Create />));
    expect(wrapper).toMatchSnapshot();
    done();
  });
});
