import React from 'react'
import FormContact from '../../molecules/form-contact/form-contact'
import { Header } from 'semantic-ui-react'

const Create = () => (
  <>
    <Header as="h2">Crea un nuevo contacto</Header>
    <FormContact />
  </>
)

export default Create