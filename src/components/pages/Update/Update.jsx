import React from 'react'
import FormContact from '../../molecules/form-contact/form-contact'
import { Header } from 'semantic-ui-react'

const Update = () => (
  <>
    <Header as="h2" >yo actualizo los datos</Header>
    <FormContact />
  </>
)

export default Update

