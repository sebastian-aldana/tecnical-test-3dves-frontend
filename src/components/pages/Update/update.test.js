import React from "react";
import { shallow } from "enzyme";
import Update from "./Update";
import withStore from "../../../store/with-store";

describe("form contact component", () => {
  test("should render component ", done => {
    const wrapper = shallow(withStore(<Update />));
    expect(wrapper).toMatchSnapshot();
    done();
  });
});
