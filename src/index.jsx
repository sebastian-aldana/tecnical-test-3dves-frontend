import React from 'react'
import ReacDOM from 'react-dom'
import App from './routes/App'
import { Provider } from 'react-redux'
import { store, history, persistor } from './store'
import { PersistGate } from 'redux-persist/integration/react'
import { ConnectedRouter } from 'connected-react-router'
import 'semantic-ui-css/semantic.min.css'

ReacDOM.render(
  <Provider store={store} >
    <PersistGate persistor={persistor} >
      <ConnectedRouter history={history} >
        <App />
      </ConnectedRouter>
    </PersistGate>
  </Provider>, document.getElementById('app'))
