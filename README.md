# 3DVES Frontend - test

Prueba tecnica desarrollador 3DVES.

### iniciar el proyecto

- clonar el repositorio:
  ```bash
  $ git clone git@gitlab.com:sebastian-aldana/tecnical-test-3dves-frontend.git
  ```
- instalar dependencias
  ```bash
  $ cd tecnical-test-3dves-frontend
  $ npm install
  ```
  
- iniciar el servidor de desarrollo

  ```bash
  $ npm run dev

  ```
  
- ingresar al navegador a localhos:8080